package com.xui.xota;

import android.os.RecoverySystem;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;

import java.io.File;
import java.io.IOException;

public class MainActivity extends AppCompatActivity {

    private Button mBtnUpdate;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (judgeUpdate()) {
            setContentView(R.layout.activity_update);
            mBtnUpdate = (Button) findViewById(R.id.btn_update);

            mBtnUpdate.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Log.d("XUI_OTA", " : Start Update!");

                    try {

                        RecoverySystem.installPackage(getApplicationContext(),
                                new File("/cache/update.zip"));
//                    RecoverySystem.rebootWipeCache(getApplication());
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            });

        } else {
            setContentView(R.layout.activity_remain);
        }

    }

    //  判断是否更新
    private boolean judgeUpdate() {
        return true;
    }

    //  获取当前版本
    private void getCurrentVersion() {

    }
}
